/***************************************************************************//**
 * @file  app.c
 * @brief Application code
 *******************************************************************************
 * # License
 * <b>Copyright 2018 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

/* Bluetooth stack headers */
#include "native_gecko.h"
#include "gatt_db.h"

/* Buttons and LEDs headers */
#include "buttons.h"
#include "leds.h"

/* Timer definitions */
#include "app_timer.h"

/* Switch app headers */
#include "switch.h"
#include "lpn.h"
#include "mesh_proxy.h"

/* Coex header */
#include "coexistence-ble.h"

/* Display Interface header */
#include "display_interface.h"

/* Retarget serial headers */
#include "retargetserial.h"
#include <stdio.h>

#ifdef ENABLE_LOGGING
#define log(...) printf(__VA_ARGS__)
#else
#define log(...)
#endif
// ------------------------------------------------------------------------ Luan
#ifdef FEATURE_I2C_SENSOR
#include "i2cspm.h"
#include "si7013.h"
#include "tempsens.h"
#endif
/***************************************************************************//**
 * @addtogroup Application
 * @{
 ******************************************************************************/

/***************************************************************************//**
 * @addtogroup app
 * @{
 ******************************************************************************/

/*******************************************************************************
 * Provisioning bearers defines.
 ******************************************************************************/
#define PB_ADV   0x1 ///< Advertising Provisioning Bearer
#define PB_GATT  0x2 ///< GATT Provisioning Bearer

/// Flag for indicating DFU Reset must be performed
uint8_t boot_to_dfu = 0;

/*******************************************************************************
 * Global variables
 ******************************************************************************/
/// Address of the Primary Element of the Node
static uint16_t _my_address = 0;
/// number of active Bluetooth connections
static uint8_t num_connections = 0;
/// handle of the last opened LE connection
static uint8_t conn_handle = 0xFF;
/// Flag for indicating that provisioning procedure is finished
static uint8_t provisioning_finished = 0;

/*******************************************************************************
 * Function prototypes.
 ******************************************************************************/
bool mesh_bgapi_listener(struct gecko_cmd_packet *evt);
static void handle_gecko_event(uint32_t evt_id, struct gecko_cmd_packet *pEvt);

/***************************************************************************//**
 * Initialise used bgapi classes.
 ******************************************************************************/
static void gecko_bgapi_classes_init(void)
{
  gecko_bgapi_class_dfu_init();
  gecko_bgapi_class_system_init();
  gecko_bgapi_class_le_gap_init();
  gecko_bgapi_class_le_connection_init();
  gecko_bgapi_class_gatt_server_init();
  gecko_bgapi_class_hardware_init();
  gecko_bgapi_class_flash_init();
  gecko_bgapi_class_test_init();
  gecko_bgapi_class_mesh_node_init();
  gecko_bgapi_class_mesh_proxy_init();
  gecko_bgapi_class_mesh_proxy_server_init();
  gecko_bgapi_class_mesh_lpn_init();
  gecko_bgapi_class_mesh_generic_client_init();
  gecko_bgapi_class_mesh_scene_client_init();
}

/*******************************************************************************
 * Main application code.
 * @param[in] pConfig  Pointer to stack configuration.
 ******************************************************************************/
void appMain(const gecko_configuration_t *pConfig)
{
  // Initialize stack
  gecko_stack_init(pConfig);
  gecko_bgapi_classes_init();

  // Initialize the random number generator which is needed for proper radio work.
  gecko_cmd_system_get_random_data(16);

  // Initialize coexistence interface. Parameters are taken from HAL config.
  gecko_initCoexHAL();

  // Initialize debug prints and display interface
  RETARGET_SerialInit();
  DI_Init();

  // Initialize LEDs and buttons. Note: some radio boards share the same GPIO
  // for button & LED. Initialization is done in this order so that default
  // configuration will be "button" for those radio boards with shared pins.
  // led_init() is called later as needed to (re)initialize the LEDs
  led_init();
  button_init();

  while (1) {
    // Event pointer for handling events
    struct gecko_cmd_packet* evt;

    // If there are no events pending then the next call to gecko_wait_event()
    // may cause device go to deep sleep.
    // Make sure that debug prints are flushed before going to sleep
    if (!gecko_event_pending()) {
      RETARGET_SerialFlush();
    }

    // Check for stack event
    evt = gecko_wait_event();

    bool pass = mesh_bgapi_listener(evt);
    if (pass) {
      handle_gecko_event(BGLIB_MSG_ID(evt->header), evt);
    }
  }
}

/***************************************************************************//**
 * Set device name in the GATT database. A unique name is generated using
 * the two last bytes from the Bluetooth address of this device. Name is also
 * displayed on the LCD.
 *
 * @param[in] pAddr  Pointer to Bluetooth address.
 ******************************************************************************/
static void set_device_name(bd_addr *pAddr)
{
  char name[20];
  uint16_t res;

  // create unique device name using the last two bytes of the Bluetooth address
  sprintf(name, "switch node %02x:%02x", pAddr->addr[1], pAddr->addr[0]);

  log("Device name: '%s'\r\n", name);

  // write device name to the GATT database
  res = gecko_cmd_gatt_server_write_attribute_value(gattdb_device_name, 0, strlen(name), (uint8_t *)name)->result;
  if (res) {
    log("gecko_cmd_gatt_server_write_attribute_value() failed, code 0x%x\r\n", res);
  }

  // show device name on the LCD
  DI_Print(name, DI_ROW_NAME);
}

/***************************************************************************//**
 * This function is called to initiate factory reset. Factory reset may be
 * initiated by keeping one of the WSTK pushbuttons pressed during reboot.
 * Factory reset is also performed if it is requested by the provisioner
 * (event gecko_evt_mesh_node_reset_id).
 ******************************************************************************/
static void initiate_factory_reset(void)
{
  log("factory reset\r\n");
  DI_Print("\n***\nFACTORY RESET\n***", DI_ROW_STATUS);

  /* if connection is open then close it before rebooting */
  if (conn_handle != 0xFF) {
    gecko_cmd_le_connection_close(conn_handle);
  }

  /* perform a factory reset by erasing PS storage. This removes all the keys and other settings
     that have been configured for this node */
  gecko_cmd_flash_ps_erase_all();
  // reboot after a small delay
  gecko_cmd_hardware_set_soft_timer(TIMER_MS_2_TIMERTICK(2000),
                                    FACTORY_RESET_TIMER,
                                    SINGLE_SHOT);
}
//--------------------------------------------------------------------------Luan
void temperatureMeasure(){
  int32_t tempData;     /* Stores the Temperature data read from the RHT sensor. */
  uint32_t rhData = 0;    /* Dummy needed for storing Relative Humidity data. */
  static int32_t DummyValue = 0l; /* This dummy value can substitute the temperature sensor value if the sensor is N/A. */

#ifdef FEATURE_I2C_SENSOR
  /* Sensor relative humidity and temperature measurement returns 0 on success, nonzero otherwise */
  if (Si7013_MeasureRHAndTemp(I2C0, SI7021_ADDR, &rhData, &tempData) != 0)
#endif
  {
    /* Use the dummy value and go between 20 and 40 if the sensor read failed.
     * The ramp-up value will be seen in the characteristic in the receiving end. */
    tempData = DummyValue + 20000l;
    DummyValue = (DummyValue + 1000l) % 21000l;
  }
  tempData = tempData/1000;
  publish_temperature(tempData);
}

/***************************************************************************//**
 * Handling of stack events. Both Bluetooth LE and Bluetooth mesh events
 * are handled here.
 * @param[in] evt_id  Incoming event ID.
 * @param[in] pEvt    Pointer to incoming event.
 ******************************************************************************/
static void handle_gecko_event(uint32_t evt_id, struct gecko_cmd_packet *pEvt)
{
  uint16_t result;
  char buf[30];

  if (NULL == pEvt) {
    return;
  }

  switch (evt_id) {
    case gecko_evt_system_boot_id:
      // check pushbutton state at startup. If either PB0 or PB1 is held down then do factory reset
      if (GPIO_PinInGet(BSP_BUTTON0_PORT, BSP_BUTTON0_PIN) == 0
#ifndef FEATURE_ONE_BUTTON
          || GPIO_PinInGet(BSP_BUTTON1_PORT, BSP_BUTTON1_PIN) == 0
#endif
          ) {
        initiate_factory_reset();
      } else {
        struct gecko_msg_system_get_bt_address_rsp_t *pAddr = gecko_cmd_system_get_bt_address();

        set_device_name(&pAddr->address);

        // Initialize Mesh stack in Node operation mode, it will generate initialized event
        result = gecko_cmd_mesh_node_init()->result;
        if (result) {
          sprintf(buf, "init failed (0x%x)", result);
          DI_Print(buf, DI_ROW_STATUS);
        }
      }
      break;

    case gecko_evt_hardware_soft_timer_id:
      switch (pEvt->data.evt_hardware_soft_timer.handle) {
        case FACTORY_RESET_TIMER:
          // reset the device to finish factory reset
          gecko_cmd_system_reset(0);
          break;

        case RESTART_TIMER:
          // restart timer expires, reset the device
          gecko_cmd_system_reset(0);
          break;

        case PROVISIONING_TIMER:
          // toggle LED to indicate the provisioning state
          if (!provisioning_finished) {
            led_set_state(LED_STATE_PROV);
          }
          break;

        case RETRANS_ONOFF_TIMER:
        case RETRANS_LIGHTNESS_TIMER:
        case RETRANS_CTL_TIMER:
        case RETRANS_SCENE_TIMER:
          handle_retrans_timer_evt(pEvt);
          break;

        case NODE_CONFIGURED_TIMER:
        case FRIEND_FIND_TIMER:
          handle_lpn_timer_evt(pEvt);
          break;
          //-------------------------------------------------------------Luan
        case TEMPERATURE_MEASUREMENT:
        	temperatureMeasure();
        	break;

        default:
          break;
      }

      break;

    case gecko_evt_mesh_node_initialized_id:
      log("node initialized\r\n");

      // Initialize generic client models
      result = gecko_cmd_mesh_generic_client_init_on_off()->result;
      if (result) {
        log("mesh_generic_client_init_on_off failed, code 0x%x\r\n", result);
      }
      result = gecko_cmd_mesh_generic_client_init_lightness()->result;
      if (result) {
        log("mesh_generic_client_init_lightness failed, code 0x%x\r\n", result);
      }
      result = gecko_cmd_mesh_generic_client_init_ctl()->result;
      if (result) {
        log("mesh_generic_client_init_ctl failed, code 0x%x\r\n", result);
      }
      result = gecko_cmd_mesh_generic_client_init_common()->result;
      if (result) {
        log("mesh_generic_client_init_common failed, code 0x%x\r\n", result);
      }

      // Initialize scene client model
      result = gecko_cmd_mesh_scene_client_init(0)->result;
      if (result) {
        log("mesh_scene_client_init failed, code 0x%x\r\n", result);
      }

      struct gecko_msg_mesh_node_initialized_evt_t *pData = (struct gecko_msg_mesh_node_initialized_evt_t *)&(pEvt->data);

      if (pData->provisioned) {
        log("node is provisioned. address:%x, ivi:%ld\r\n", pData->address, pData->ivi);

        _my_address = pData->address;

        enable_button_interrupts();
        switch_node_init();

        // Initialize Low Power Node functionality
        lpn_init();

        DI_Print("provisioned", DI_ROW_STATUS);
        //---------------------------------------------------------------------------Luan
        gecko_cmd_hardware_set_soft_timer(TIMER_MS_2_TIMERTICK(2000), TEMPERATURE_MEASUREMENT, REPEATING);
      } else {
        log("node is unprovisioned\r\n");
        DI_Print("unprovisioned", DI_ROW_STATUS);

        log("starting unprovisioned beaconing...\r\n");
        // Enable ADV and GATT provisioning bearer
        gecko_cmd_mesh_node_start_unprov_beaconing(PB_ADV | PB_GATT);
      }
      break;

    case gecko_evt_system_external_signal_id:
    {
      if (pEvt->data.evt_system_external_signal.extsignals & EXT_SIGNAL_PB0_SHORT_PRESS) {
        // Handling of button press less than 0.25s
#ifdef FEATURE_ONE_BUTTON
        change_lightness(INCREASE);
#else
        change_lightness(DECREASE);
#endif
      }
#ifndef FEATURE_ONE_BUTTON
      if (pEvt->data.evt_system_external_signal.extsignals & EXT_SIGNAL_PB1_SHORT_PRESS) {
        // Handling of button press less than 0.25s
        change_lightness(INCREASE);
      }
#endif
      if (pEvt->data.evt_system_external_signal.extsignals & EXT_SIGNAL_PB0_MEDIUM_PRESS) {
        // Handling of button press greater than 0.25s and less than 1s
#ifdef FEATURE_ONE_BUTTON
        change_temperature(INCREASE);
#else
        change_temperature(DECREASE);
#endif
      }
#ifndef FEATURE_ONE_BUTTON
      if (pEvt->data.evt_system_external_signal.extsignals & EXT_SIGNAL_PB1_MEDIUM_PRESS) {
        // Handling of button press greater than 0.25s and less than 1s
        change_temperature(INCREASE);
      }
#endif
      if (pEvt->data.evt_system_external_signal.extsignals & EXT_SIGNAL_PB0_LONG_PRESS) {
        // Handling of button press greater than 1s and less than 5s
#ifdef FEATURE_ONE_BUTTON
        change_switch_position(TOGGLE);
#else
        change_switch_position(OFF);
#endif
      }
#ifndef FEATURE_ONE_BUTTON
      if (pEvt->data.evt_system_external_signal.extsignals & EXT_SIGNAL_PB1_LONG_PRESS) {
        // Handling of button press greater than 1s and less than 5s
        change_switch_position(ON);
      }
#endif
      if (pEvt->data.evt_system_external_signal.extsignals & EXT_SIGNAL_PB0_VERY_LONG_PRESS) {
        // Handling of button press greater than 5s
        select_scene(1);
      }
#ifndef FEATURE_ONE_BUTTON
      if (pEvt->data.evt_system_external_signal.extsignals & EXT_SIGNAL_PB1_VERY_LONG_PRESS) {
        // Handling of button press greater than 5s
        select_scene(2);
      }
#endif
    }
    break;

    case gecko_evt_mesh_node_provisioning_started_id:
      log("Started provisioning\r\n");
      DI_Print("provisioning...", DI_ROW_STATUS);
#ifdef FEATURE_LED_BUTTON_ON_SAME_PIN
      led_init(); /* shared GPIO pins used as LED output */
#endif
      // start timer for blinking LEDs to indicate which node is being provisioned
      gecko_cmd_hardware_set_soft_timer(TIMER_MS_2_TIMERTICK(250),
                                        PROVISIONING_TIMER,
                                        REPEATING);
      break;

    case gecko_evt_mesh_node_provisioned_id:
      provisioning_finished = 1;
      switch_node_init();
      // try to initialize lpn after 30 seconds, if no configuration messages come
      set_configuration_timer(30000);
      log("node provisioned, got address=%x\r\n", pEvt->data.evt_mesh_node_provisioned.address);
      // stop LED blinking when provisioning complete
      gecko_cmd_hardware_set_soft_timer(TIMER_STOP,
                                        PROVISIONING_TIMER,
                                        REPEATING);
      led_set_state(LED_STATE_OFF);
      DI_Print("provisioned", DI_ROW_STATUS);

#ifdef FEATURE_LED_BUTTON_ON_SAME_PIN
      button_init(); /* shared GPIO pins used as button input */
#endif
      enable_button_interrupts();

      //---------------------------------------------------------------------------Luan
      gecko_cmd_hardware_set_soft_timer(TIMER_MS_2_TIMERTICK(2000), TEMPERATURE_MEASUREMENT, REPEATING);
      break;

    case gecko_evt_mesh_node_provisioning_failed_id:
      log("provisioning failed, code 0x%x\r\n", pEvt->data.evt_mesh_node_provisioning_failed.result);
      DI_Print("prov failed", DI_ROW_STATUS);
      /* start a one-shot timer that will trigger soft reset after small delay */
      gecko_cmd_hardware_set_soft_timer(TIMER_MS_2_TIMERTICK(2000),
                                        RESTART_TIMER,
                                        SINGLE_SHOT);
      break;

    case gecko_evt_mesh_node_key_added_id:
      log("got new %s key with index 0x%x\r\n",
          pEvt->data.evt_mesh_node_key_added.type == 0 ? "network" : "application",
          pEvt->data.evt_mesh_node_key_added.index);
      // try to init lpn 5 seconds after adding key
      set_configuration_timer(5000);
      break;

    case gecko_evt_mesh_node_model_config_changed_id:
      log("model config changed\r\n");
      // try to init lpn 5 seconds after configuration change
      set_configuration_timer(5000);
      break;

    case gecko_evt_mesh_node_config_set_id:
      log("model config set\r\n");
      // try to init lpn 5 seconds after configuration set
      set_configuration_timer(5000);
      break;

    case gecko_evt_le_connection_opened_id:
      log("evt:gecko_evt_le_connection_opened_id\r\n");
      num_connections++;
      conn_handle = pEvt->data.evt_le_connection_opened.connection;
      DI_Print("connected", DI_ROW_CONNECTION);
      break;

    case gecko_evt_le_connection_closed_id:
      /* Check if need to boot to dfu mode */
      if (boot_to_dfu) {
        /* Enter to DFU OTA mode */
        gecko_cmd_system_reset(2);
      }

      log("evt:conn closed, reason 0x%x\r\n", pEvt->data.evt_le_connection_closed.reason);
      conn_handle = 0xFF;
      if (num_connections > 0) {
        if (--num_connections == 0) {
          DI_Print("", DI_ROW_CONNECTION);
        }
      }
      break;

    case gecko_evt_mesh_node_reset_id:
      log("evt gecko_evt_mesh_node_reset_id\r\n");
      initiate_factory_reset();
      break;

    case gecko_evt_le_connection_parameters_id:
      log("connection params: interval %d, timeout %d\r\n",
          pEvt->data.evt_le_connection_parameters.interval,
          pEvt->data.evt_le_connection_parameters.timeout);
      break;

    case gecko_evt_le_gap_adv_timeout_id:
      // these events silently discarded
      break;

    case gecko_evt_gatt_server_user_write_request_id:
      if (pEvt->data.evt_gatt_server_user_write_request.characteristic == gattdb_ota_control) {
        /* Set flag to enter to OTA mode */
        boot_to_dfu = 1;
        /* Send response to Write Request */
        gecko_cmd_gatt_server_send_user_write_response(
          pEvt->data.evt_gatt_server_user_write_request.connection,
          gattdb_ota_control,
          bg_err_success);

        /* Close connection to enter to DFU OTA mode */
        gecko_cmd_le_connection_close(pEvt->data.evt_gatt_server_user_write_request.connection);
      }
      break;

    case gecko_evt_mesh_proxy_connected_id:
    case gecko_evt_mesh_proxy_disconnected_id:
      handle_mesh_proxy_events(pEvt);
      break;

    case gecko_evt_mesh_lpn_friendship_established_id:
    case gecko_evt_mesh_lpn_friendship_failed_id:
    case gecko_evt_mesh_lpn_friendship_terminated_id:
      handle_lpn_events(pEvt);
      break;

    default:
      //log("unhandled evt: %8.8x class %2.2x method %2.2x\r\n", evt_id, (evt_id >> 16) & 0xFF, (evt_id >> 24) & 0xFF);
      break;
  }
}

/** @} (end addtogroup app) */
/** @} (end addtogroup Application) */
