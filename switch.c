/***************************************************************************//**
 * @file  switch.c
 * @brief Switch module implementation
 *******************************************************************************
 * # License
 * <b>Copyright 2020 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

/* C Standard Library headers */
#include <stdlib.h>
#include <stdio.h>

/* Bluetooth stack headers */
#include "native_gecko.h"
#include "mesh_generic_model_capi_types.h"
#include "mesh_lighting_model_capi_types.h"
#include "mesh_lib.h"
#include "board_features.h"

/* Timer definitions */
#include "app_timer.h"

/* Own header */
#include "switch.h"

#ifdef ENABLE_LOGGING
#define log(...) printf(__VA_ARGS__)
#else
#define log(...)
#endif

#include <math.h>
/***************************************************************************//**
 * @addtogroup Switch
 * @{
 ******************************************************************************/

#define SWITCH_ELEMENT     0 ///< Client models located in primary element
#define IMMEDIATE          0 ///< Immediate transition time is 0 seconds
#define PUBLISH_ADDRESS    0 ///< The unused 0 address is used for publishing
#define IGNORED            0 ///< Parameter ignored for publishing
#define NO_FLAGS           0 ///< No flags used for message

#define TEMPERATURE_MIN  0x0320 ///< Minimum color temperature 800K
#define TEMPERATURE_MAX  0x4e20 ///< Maximum color temperature 20000K
#define DELTA_UV         0      ///< Delta UV is hardcoded to 0 in this example

/// current position of the switch
static uint8_t switch_pos = 0;
/// number of on/off requests to be sent
static uint8_t onoff_request_count;
/// on/off transaction identifier
static uint8_t onoff_trid = 0;
/// lightness level percentage
static uint8_t lightness_percent = 0;
/// lightness level converted from percentage to actual value, range 0..65535
static uint16_t lightness_level = 0;
/// number of lightness requests to be sent
static uint8_t lightness_request_count;
/// lightness transaction identifier
static uint8_t lightness_trid = 0;
/// temperature level percentage
static uint8_t temperature_percent = 50;
/// temperature level converted from percentage to actual value, range 0..65535
static uint16_t temperature_level = 0;
/// number of ctl requests to be sent
static uint8_t ctl_request_count;
/// ctl transaction identifier
static uint8_t ctl_trid = 0;
/// currently selected scene
static uint16_t scene_number = 0;
/// number of scene requests to be sent
static uint8_t scene_request_count;
/// scene transaction identifier
static uint8_t scene_trid = 0;

/*******************************************************************************
 * Switch node initialization.
 * This is called at each boot if provisioning is already done.
 * Otherwise this function is called after provisioning is completed.
 ******************************************************************************/
void switch_node_init(void)
{
  // Initialize mesh lib, up to 8 models
  mesh_lib_init(malloc, free, 8);
}

/***************************************************************************//**
 * This function publishes one generic on/off request to change the state
 * of light(s) in the group. Global variable switch_pos holds the latest
 * desired light state, possible values are:
 * switch_pos = 1 -> PB1 was pressed long (above 1s), turn lights on
 * switch_pos = 0 -> PB0 was pressed long (above 1s), turn lights off
 *
 * param[in] retrans  Indicates if this is the first request or a retransmission,
 *                    possible values are 0 = first request, 1 = retransmission.
 *
 * @note This application sends multiple generic on/off requests for each
 *       long button press to improve reliability.
 *       The transaction ID is not incremented in case of a retransmission.
 ******************************************************************************/
static void send_onoff_request(uint8_t retrans)
{
  struct mesh_generic_request req;
  const uint32_t transtime = 0; // using zero transition time by default

  req.kind = mesh_generic_request_on_off;
  req.on_off = switch_pos ? MESH_GENERIC_ON_OFF_STATE_ON : MESH_GENERIC_ON_OFF_STATE_OFF;

  // Increment transaction ID for each request, unless it's a retransmission
  if (retrans == 0) {
    onoff_trid++;
  }

  // Delay for the request is calculated so that the last request will have
  // a zero delay and each of the previous request have delay that increases
  // in 50 ms steps. For example, when using three on/off requests
  // per button press the delays are set as 100, 50, 0 ms
  uint16_t delay = (onoff_request_count - 1) * 50;

  uint16_t resp = mesh_lib_generic_client_publish(MESH_GENERIC_ON_OFF_CLIENT_MODEL_ID,
                                                  SWITCH_ELEMENT,
                                                  onoff_trid,
                                                  &req,
                                                  transtime, // transition time in ms
                                                  delay,
                                                  NO_FLAGS   // flags
                                                  );

  if (resp) {
    log("gecko_cmd_mesh_generic_client_publish failed, code 0x%x\r\n", resp);
  } else {
    log("on/off request sent, trid = %u, delay = %u\r\n", onoff_trid, delay);
  }

  // Keep track of how many requests has been sent
  if (onoff_request_count > 0) {
    onoff_request_count--;
  }
}

/***************************************************************************//**
 * This function publishes one light lightness request to change the lightness
 * level of light(s) in the group. Global variable lightness_level holds
 * the latest desired light level.
 *
 * param[in] retrans  Indicates if this is the first request or a retransmission,
 *                    possible values are 0 = first request, 1 = retransmission.
 *
 * @note This application sends multiple lightness requests for each
 *       short button press to improve reliability.
 *       The transaction ID is not incremented in case of a retransmission.
 ******************************************************************************/
static void send_lightness_request(uint8_t retrans)
{
  struct mesh_generic_request req;

  req.kind = mesh_lighting_request_lightness_actual;
  req.lightness = lightness_level;

  // Increment transaction ID for each request, unless it's a retransmission
  if (retrans == 0) {
    lightness_trid++;
  }

  // Delay for the request is calculated so that the last request will have
  // a zero delay and each of the previous request have delay that increases
  // in 50 ms steps. For example, when using three lightness requests
  // per button press the delays are set as 100, 50, 0 ms
  uint16_t delay = (lightness_request_count - 1) * 50;

  uint16_t resp = mesh_lib_generic_client_publish(MESH_LIGHTING_LIGHTNESS_CLIENT_MODEL_ID,
                                                  SWITCH_ELEMENT,
                                                  lightness_trid,
                                                  &req,
                                                  IMMEDIATE,     // transition
                                                  delay,
                                                  NO_FLAGS       // flags
                                                  );

  if (resp) {
    log("gecko_cmd_mesh_generic_client_publish failed, code 0x%x\r\n", resp);
  } else {
    log("lightness request sent, trid = %u, delay = %u\r\n",
        lightness_trid,
        delay);
  }

  // Keep track of how many requests has been sent
  if (lightness_request_count > 0) {
    lightness_request_count--;
  }
}

/***************************************************************************//**
 * This function publishes one light CTL request to change the temperature level
 * of light(s) in the group. Global variable temperature_level holds the latest
 * desired light temperature level.
 * The CTL request also send lightness_level which holds the latest desired light
 * lightness level and Delta UV which is hardcoded to 0 for this application.
 *
 * param[in] retrans  Indicates if this is the first request or a retransmission,
 *                    possible values are 0 = first request, 1 = retransmission.
 *
 * @note This application sends multiple ctl requests for each
 *       medium button press to improve reliability.
 *       The transaction ID is not incremented in case of a retransmission.
 ******************************************************************************/
static void send_ctl_request(uint8_t retrans)
{
  struct mesh_generic_request req;

  req.kind = mesh_lighting_request_ctl;
  req.ctl.lightness = lightness_level;
  req.ctl.temperature = temperature_level;
  req.ctl.deltauv = DELTA_UV; //hardcoded delta uv

  // Increment transaction ID for each request, unless it's a retransmission
  if (retrans == 0) {
    ctl_trid++;
  }

  // Delay for the request is calculated so that the last request will have
  // a zero delay and each of the previous request have delay that increases
  // in 50 ms steps. For example, when using three ctl requests
  // per button press the delays are set as 100, 50, 0 ms
  uint16_t delay = (ctl_request_count - 1) * 50;

  uint16_t resp = mesh_lib_generic_client_publish(MESH_LIGHTING_CTL_CLIENT_MODEL_ID,
                                                  SWITCH_ELEMENT,
                                                  ctl_trid,
                                                  &req,
                                                  IMMEDIATE,     // transition
                                                  delay,
                                                  NO_FLAGS       // flags
                                                  );

  if (resp) {
    log("gecko_cmd_mesh_generic_client_publish failed, code 0x%x\r\n", resp);
  } else {
    log("ctl request sent, trid = %u, delay = %u\r\n", ctl_trid, delay);
  }

  // Keep track of how many requests has been sent
  if (ctl_request_count > 0) {
    ctl_request_count--;
  }
}

/***************************************************************************//**
 * This function publishes one scene recall request to recall selected scene.
 * Global variable scene_number holds the latest desired scene state.
 *
 * param[in] retrans  Indicates if this is the first request or a retransmission,
 *                    possible values are 0 = first request, 1 = retransmission.
 *
 * @note This application sends multiple scene requests for each
 *       very long button press to improve reliability.
 *       The transaction ID is not incremented in case of a retransmission.
 ******************************************************************************/
static void send_scene_recall_request(uint8_t retrans)
{
  // Increment transaction ID for each request, unless it's a retransmission
  if (retrans == 0) {
    scene_trid++;
  }

  // Delay for the request is calculated so that the last request will have
  // a zero delay and each of the previous request have delay that increases
  // in 50 ms steps. For example, when using three scene requests
  // per button press the delays are set as 100, 50, 0 ms
  uint16_t delay = (scene_request_count - 1) * 50;

  uint16_t resp = gecko_cmd_mesh_scene_client_recall(SWITCH_ELEMENT,
                                                     PUBLISH_ADDRESS,
                                                     IGNORED,
                                                     NO_FLAGS,
                                                     scene_number,
                                                     scene_trid,
                                                     IMMEDIATE,
                                                     delay
                                                     )->result;

  if (resp) {
    log("gecko_cmd_scene_client_recall failed, code 0x%x\r\n", resp);
  } else {
    log("scene request sent, trid = %u, delay = %u\r\n", scene_trid, delay);
  }

  // Keep track of how many requests has been sent
  if (scene_request_count > 0) {
    scene_request_count--;
  }
}

/*******************************************************************************
 * This function change the lightness and send it to the server.
 *
 * @param[in] change  Defines lightness change, possible values are
 *                    0 = decrease by 10%, 1 = increase by 10%.
 *
 ******************************************************************************/
void change_lightness(uint8_t change)
{
  // Adjust light brightness, using Light Lightness model
  if (change == INCREASE) {
    lightness_percent += 10;
    if (lightness_percent > 100) {
#ifdef FEATURE_ONE_BUTTON
      lightness_percent = 0;
#else
      lightness_percent = 100;
#endif
    }
  } else {
    if (lightness_percent >= 10) {
      lightness_percent -= 10;
    }
  }

  lightness_level = lightness_percent * 0xFFFF / 100;
  log("set light to %u %% / level %u\r\n", lightness_percent, lightness_level);

  lightness_request_count = 3; // Request is sent 3 times to improve reliability

  send_lightness_request(0);  // Send the first request

  // If there are more requests to send, start a repeating soft timer
  // to trigger retransmission of the request after 50 ms delay
  if (lightness_request_count > 0) {
    gecko_cmd_hardware_set_soft_timer(TIMER_MS_2_TIMERTICK(50),
                                      RETRANS_LIGHTNESS_TIMER,
                                      REPEATING);
  }
}

/*******************************************************************************
 * This function change the temperature and send it to the server.
 *
 * @param[in] change  Defines temperature change, possible values are
 *                    0 = decrease by 10%, 1 = increase by 10%.
 *
 ******************************************************************************/
void change_temperature(uint8_t change)
{
  // Adjust light temperature, using Light CTL model
  if (change == INCREASE) {
    temperature_percent += 10;
    if (temperature_percent > 100) {
#ifdef FEATURE_ONE_BUTTON
      temperature_percent = 0;
#else
      temperature_percent = 100;
#endif
    }
  } else {
    if (temperature_percent >= 10) {
      temperature_percent -= 10;
    }
  }
  // Using square of percentage to change temperature more uniformly just for demonstration
  temperature_level = TEMPERATURE_MIN                                     \
                      + (temperature_percent * temperature_percent / 100) \
                      * (TEMPERATURE_MAX - TEMPERATURE_MIN)               \
                      / 100;
  log("set temperature to %u %% / level %u K\r\n",
      temperature_percent * temperature_percent / 100,
      temperature_level);

  ctl_request_count = 3; // Request is send 3 times to improve reliability

  send_ctl_request(0);  //Send the first request

  // If there are more requests to send, start a repeating soft timer
  // to trigger retransmission of the request after 50 ms delay
  if (ctl_request_count > 0) {
    gecko_cmd_hardware_set_soft_timer(TIMER_MS_2_TIMERTICK(50),
                                      RETRANS_CTL_TIMER,
                                      REPEATING);
  }
}
//----------------------------------------------------------------------Luan
void publish_temperature(uint32_t temperature){
	// Using square of percentage to change temperature more uniformly just for demonstration
	temperature_percent = temperature;
	temperature_level = TEMPERATURE_MIN
					+ (temperature_percent * temperature_percent / 100)

	ctl_request_count = 3; // Request is send 3 times to improve reliability
	send_ctl_request(0);  //Send the first request
	// If there are more requests to send, start a repeating soft timer
	// to trigger retransmission of the request after 50 ms delay
	if (ctl_request_count > 0) {
		gecko_cmd_hardware_set_soft_timer(TIMER_MS_2_TIMERTICK(50),
				RETRANS_CTL_TIMER,
				REPEATING);
  }
}
/*******************************************************************************
 * This function change the switch position and send it to the server.
 *
 * @param[in] position  Defines switch position change, possible values are
 *                      0 = OFF, 1 = ON, 2 = TOGGLE.
 *
 ******************************************************************************/
void change_switch_position(uint8_t position)
{
  if (position != TOGGLE) {
    switch_pos = position;
  } else {
    switch_pos = 1 - switch_pos; // Toggle switch state
  }

  // Turns light ON or OFF, using Generic OnOff model
  log("turn light(s)");
  if (switch_pos) {
    log("on\r\n");
    lightness_percent = 100;
  } else {
    log("off\r\n");
    lightness_percent = 0;
  }

  onoff_request_count = 3; // Request is sent 3 times to improve reliability

  send_onoff_request(0);  // Send the first request

  // If there are more requests to send, start a repeating soft timer
  // to trigger retransmission of the request after 50 ms delay
  if (onoff_request_count > 0) {
    gecko_cmd_hardware_set_soft_timer(TIMER_MS_2_TIMERTICK(50),
                                      RETRANS_ONOFF_TIMER,
                                      REPEATING);
  }
}

/*******************************************************************************
 * This function select scene and send it to the server.
 *
 * @param[in] scene_to_recall  Scene to recall, possible values 1-255.
 *
 ******************************************************************************/
void select_scene(uint8_t scene_to_recall)
{
  // Scene number 0 is prohibited
  if (scene_to_recall == 0) {
    return;
  }

  scene_number = scene_to_recall;

  // Recall scene using Scene Client model
  log("Recall scene number %u\r\n", scene_number);

  scene_request_count = 3; // Request is sent 3 times to improve reliability

  send_scene_recall_request(0);  // Send the first request

  // If there are more requests to send, start a repeating soft timer
  // to trigger retransmission of the request after 50 ms delay
  if (scene_request_count > 0) {
    gecko_cmd_hardware_set_soft_timer(TIMER_MS_2_TIMERTICK(50),
                                      RETRANS_SCENE_TIMER,
                                      REPEATING);
  }
}

/*******************************************************************************
 *  Handling of message retransmission timer events.
 *
 *  @param[in] pEvt  Pointer to incoming event.
 ******************************************************************************/
void handle_retrans_timer_evt(struct gecko_cmd_packet *pEvt)
{
  switch (pEvt->data.evt_hardware_soft_timer.handle) {
    case RETRANS_ONOFF_TIMER:
      send_onoff_request(1);   // param 1 indicates that this is a retransmission
      // stop retransmission timer if it was the last attempt
      if (onoff_request_count == 0) {
        gecko_cmd_hardware_set_soft_timer(TIMER_STOP,
                                          RETRANS_ONOFF_TIMER,
                                          REPEATING);
      }
      break;

    case RETRANS_LIGHTNESS_TIMER:
      send_lightness_request(1);   // Retransmit lightness message
      // Stop retransmission timer if it was the last attempt
      if (lightness_request_count == 0) {
        gecko_cmd_hardware_set_soft_timer(TIMER_STOP,
                                          RETRANS_LIGHTNESS_TIMER,
                                          REPEATING);
      }
      break;

    case RETRANS_CTL_TIMER:
      send_ctl_request(1);   // Retransmit ctl message
      // Stop retransmission timer if it was the last attempt
      if (ctl_request_count == 0) {
        gecko_cmd_hardware_set_soft_timer(TIMER_STOP,
                                          RETRANS_CTL_TIMER,
                                          REPEATING);
      }
      break;

    case RETRANS_SCENE_TIMER:
      send_scene_recall_request(1);   // Retransmit scene message
      // Stop retransmission timer if it was the last attempt
      if (scene_request_count == 0) {
        gecko_cmd_hardware_set_soft_timer(TIMER_STOP,
                                          RETRANS_SCENE_TIMER,
                                          REPEATING);
      }
      break;

    default:
      break;
  }
}

/** @} (end addtogroup Switch) */
