/***************************************************************************//**
 * @file  switch.h
 * @brief Switch header file
 *******************************************************************************
 * # License
 * <b>Copyright 2020 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#ifndef SWITCH_H
#define SWITCH_H

#include <stdint.h>
#include "native_gecko.h"

/***************************************************************************//**
 * \defgroup Switch
 * \brief Switch state and associated mesh models.
 ******************************************************************************/

/***************************************************************************//**
 * @addtogroup Switch
 * @{
 ******************************************************************************/

/*******************************************************************************
   Public Macros and Definitions
*******************************************************************************/
#define DECREASE    0   ///< Decrease value
#define INCREASE    1   ///< Increase value
#define OFF         0   ///< Set switch state to off
#define ON          1   ///< Set switch state to on
#define TOGGLE      2   ///< Toggle switch state

/***************************************************************************//**
 * Switch node initialization.
 * This is called at each boot if provisioning is already done.
 * Otherwise this function is called after provisioning is completed.
 ******************************************************************************/
void switch_node_init(void);

/***************************************************************************//**
 * This function change the lightness and send it to the server.
 *
 * @param[in] change  Defines lightness change, possible values are
 *                    0 = decrease by 10%, 1 = increase by 10%.
 *
 ******************************************************************************/
void change_lightness(uint8_t change);

/***************************************************************************//**
 * This function change the temperature and send it to the server.
 *
 * @param[in] change  Defines temperature change, possible values are
 *                    0 = decrease by 10%, 1 = increase by 10%.
 *
 ******************************************************************************/
void change_temperature(uint8_t change);
void publish_temperature(uint32_t temperature);

/***************************************************************************//**
 * This function change the switch position and send it to the server.
 *
 * @param[in] position  Defines switch position change, possible values are
 *                      0 = OFF, 1 = ON, 2 = TOGGLE.
 *
 ******************************************************************************/
void change_switch_position(uint8_t position);

/***************************************************************************//**
 * This function select scene and send it to the server.
 *
 * @param[in] scene_to_recall  Scene to recall, possible values 1-255.
 *
 ******************************************************************************/
void select_scene(uint8_t scene_to_recall);

/***************************************************************************//**
 *  Handling of message retransmission timer events.
 *
 *  @param[in] pEvt  Pointer to incoming event.
 ******************************************************************************/
void handle_retrans_timer_evt(struct gecko_cmd_packet *pEvt);

/** @} (end addtogroup Switch) */

#endif /* SWITCH_H */
